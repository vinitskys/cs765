# Had to do some manual removal too

inPath = "census.csv"
outPath = "census_states.csv"

inFile = open(inPath, 'r')
outFile = open(outPath, 'w')

for line in inFile:
    first = line.split(',')[0]
    if first == "FIPS":
        continue
    first = int(first)
    if first % 100 == 0:
        outFile.write(line)