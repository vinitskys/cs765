1. Descriptions of the Designs and Their Intents:

For this project, I was interested in the task of comparing the distribution of product reviews between different categories. For example, do CD's tend to get better reviews than records? One approach to this would be using some kind of summary statistic -- for example, computing the average review score for each product. Then we could compare the distribution of average reviews across categories (using faceted 1-dimensional scatterplots, for example). However, this does not tell the whole story! A product that got all 3-star reviews is VERY different from a product that got an equal number of 5- and 1-star reviews and none in the middle (the first is very average, the second is highly polarizing/possible defective). This means that we actually care about product's review distribution -- meaning we need to compare distributions of distributions, a much more challenging and intriguing task! 

One caveat here is the number of products -- there are hundreds or thousands of products in each category, so including every product in our visualization may not be possible. Furthermore, products with few reviews may not have very interesting distributions, or the distributions might not be a meaningful sample of people's opinions on the product. In order to counteract this, I chose to only consider the 30 products with the most reviews in each category -- these will certainly have a critical mass of reviews.

In light of the discussion above, the specific task I settled on was "how do the distributions of the 30 most-reviewed products compare across categories". Abstractly, this could be stated as "comparing collections of ordinal frequency data", which sounds pretty cool. 

Since we are comparing, one consideration was whether I wanted to use a faceted or overlaid layout. I chose a faceted layout so trends within one category would be easier to see -- the user could subsequently compare these trends to those found in the other category. If I had overlaid the two categories on a single region, it would have been easier to compare item-by-item, but since our goal is looking at the distributions, I decided that seeing trends within one category was more important.

Once I chose a faceted layout, it seemed natural to encode the product a position on the X-axis, with the products sorted by number of reviews (most-reviewed on the left), with the review SOMEHOW encoded along the Y-axis (more on that later!). This way it was easy to see trends of how review distributions changed as the products rank (number of reviews) changed. Similarly, it would be easy for the viewer to mentally isolate out the top X items from each category and just compare the trends among those (i.e. the top 1, top 5, or top 10).

The hardest decision was how to encode the review distributions along the Y-axis for each product. One option would be to use a box-and-whiskers plot -- show the median and the quartiles (maybe with the average as well?). However, since reviews are ordinal with only 5 categories, this didn't seem to make much sense. Another option would have been to plot the average and standard deviation -- this would have fixed the issue I mentioned earlier of losing the spread of the reviews. Perhaps the average could be encoded as a dark point, and the standard deviation as the length fo a lighter vertical line on either side of the point. However, standard deviation is not intuitive, and any visualization of the "spread" of the item's distribution would lose some interpretability and intuitiveness. Instead, I decided to use a simpler design -- each product's review would be encoded as a vertical grid of 5 boxes, with each box encoding (somehow) the how many reviews of that score the product got. Since we care about review distributions, it seem the percentages of each score rather than the raw numbers -- this alleviated any issues of comparing products with different numbers of reviews either between or across categories, and makes trends easier to spot between categories (even if one category is far more frequently reviewed). The last question was how to encode the percentage of reviews in each box. Any length/size encoding didn't make sense because of the structured grid layout, and it seemed natural to encode the percentage of reviews as the "color" of the box. This way each category's facet would be a grid of colored squares -- very easy to spot trends in! I spent a long time playing around with color schemes for this (this was before talked about color in class!) After a few hours I settled on a lightness/saturation gradient in red. Then in class we discussed color (and ColorBrewer), so I hooped over there are borrowed a 9-category multi-hue color ramp. This way it would be very easy to match colors across categories/rank (something my original scale was very bad for), and very easy to see trends within and between/categories.

At this point, the visualization was missing a vital piece -- information about tha products themselves! It seemed infeasible to put the name of each product on the X-axis, so I decided to use details-on-demand to give the use the name of the product when they hovered the mouse over it. This is useful for anomaly detection and being able to interpret the data. In addition, I included the price of the product, in case the user is interested in seeing price trends as well. Since the colors ramp is discrete, it loses some information (for example, 1% and 10% are the same color...). In order to remedy this, I also decided to include the actual percentages of each level of review in this popup. I'm honestly not sure whether this was a good decision, as it may just cause unnecessary clutter on the screen, and may not really give much insight (the user is unlikely to hover over to get this information).

In order to allow the user to compare across different categories, I put in a selection bar for each facet that allows the user to select any of the datasets that have been put into the visualization on the backend. This allows the user to compare trends between any categories they want to. In order to keep the comparison simple, I decided to only include two facets, and have the user do pair-wise comparison (additionally, screen real estate issues meant I couldn't use more than two facets and still keep the boxes a reasonable size).

*TODO:SCREENSHOTS*

---------------------------------------------------------------------------------------------------

2. Use Case Evaluation: Show examples (e.g., screenshots with descriptions) that show that your designs really address the tasks that they are meant to address. (this is part of 1, but is so important that I emphasize it)

---------------------------------------------------------------------------------------------------

3. A Scalability Discussion:

This design scales very very well to larger datasets. If a dataset has a billion items, we still only look at the top 30! Maybe this subsetting missing some trends in the data, but at least we can still see trends in the most-reviewed items. Since we're only comparing two categories at a time, this design scales well to a larger number of categories as well! It also scales very well to a massive number of reviews, since we are using percentages rather than counts to encode the "number of reviews" of each level. The implementation I used would also scale well to any of these parameters without much adjustment.

---------------------------------------------------------------------------------------------------

4. Information about the programs: This must include requirements (what languages and libraries) and instructions on how to run the program.

The program is a standalone HTML page. It uses d3 from the web, so you need a web connection to run it (something I realized while trying to work on this writeup on the bus to Minneapolis, haha...). The backend data wrangling uses standard Python 3, nothing fancy.

*TODO:WEB PAGE?*

---------------------------------------------------------------------------------------------------

5. Interaction: Be clear whether or not your visualizations are interactive. If they are interactive, describe the interactivity well enough that we can imagine how things work without actually running your program. Use lots of pictures. You may also add a video (not in the PDF).

This visualization is interactive! The user can hover over a product to see its name, price, and review percentages. The user can also use the selection menu to pick which categories to compare.

*TODO:VIDEO* 

---------------------------------------------------------------------------------------------------

6. Data Sets: Be clear what data sets you use in your examples (any images and descriptions you turn in). Let us know if your program can read other data sets, and what its limitations are.

I used the provided datasets for musical instruments and CD/vinyls. This program can be extended to other categories/datasets with minimal effort -- all you have to do is re-run the python script on the backend, and copy-paste the results into the javascript file!

---------------------------------------------------------------------------------------------------

7. Self-Assessment: Please give your honest assessment of your familiarity with the tools that you used, and how much of your energy for this project went into learning those tools. For example, you might say “I am an experienced Python programmer, and used to having to learn new APIs, so picking up Bokeh wasn’t a big deal” or “I had never done any JavaScript programming, so I spend a ton of time working through a lot of tutorials to learn D3”.

I had some familiarity with HTML/javascript from Graphics, so using it was pretty easy. However, learning d3 had a pretty steep learning curve, and if we're being honest I still kinda don't get why some of the things I did works... But sometimes programming is just adding/removing random lines of code until it looks right, ya know? I chose Python for the backend stuff because it is my go-to language for everything, so that was pretty simple.