import sys
import csv

# read csv into a dictionary
file = open("data/" + sys.argv[1] + ".csv", "r")
ratings = {}
for line in file:
    
    line = line.split(",")
    name = line[0]
    score = line[2]
    
    if name not in ratings:
        ratings[name] = []
    else:
        ratings[name].append(score)

# figure out the 30 most frequently reviewed
best = []

for name in ratings:
    
    if name == "asin":
        continue

    if len(best) == 0:
        best.append(name)
    
    # if we don't yet have 30, just add it where it goes
    if len(best) < 30:
        for i in range(len(best)):
            if len(ratings[name]) > len(ratings[best[i]]):
                best.insert(i, name)
                break
    else:
        for i in range(len(best)):
            if len(ratings[name]) > len(ratings[best[i]]):
                best.insert(i, name)
                best = best[:-1]
                break

# calculate the number of each rating
ratingCounts = []
for name in best:
    myRatings = [0,0,0,0,0]
    for review in ratings[name]:
        myRatings[int(float(review))-1] += 1
    ratingCounts.append(myRatings)

# convert to percentages
for dist in ratingCounts:
    thisSum = sum(dist)
    for i in range(len(dist)):
        dist[i] = dist[i] / thisSum
        dist[i] = int(100*dist[i])

# print out nicely
print(ratingCounts)

metaFile = open("data/" + sys.argv[1] + "_meta.csv", "r")

reader = csv.reader(metaFile)

meta = []
for line in reader:
    for name in best:
        if line[0] == name:
            meta.append([line[1],line[2]])

print(meta)


