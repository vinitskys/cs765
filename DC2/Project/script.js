// script.js
// Make the visualization

function load(){

    // clear the svg
    d3.selectAll("svg").selectAll("*").remove();

    // each entry is one column of ratings
    var instrumentsScores = [[3, 0, 2, 11, 82], [0, 2, 5, 22, 69], [0, 1, 4, 20, 73], [0, 1, 8, 12, 76], [1, 3, 3, 14, 78], [1, 2, 5, 26, 64], [2, 2, 5, 20, 68], [1, 1, 8, 23, 64], [1, 1, 7, 25, 64], [4, 1, 13, 12, 68], [3, 1, 7, 28, 59], [1, 3, 20, 27, 46], [1, 3, 4, 14, 75], [7, 1, 1, 21, 68], [0, 1, 3, 17, 77], [0, 1, 8, 26, 62], [0, 3, 5, 25, 66], [0, 1, 0, 12, 85], [5, 1, 9, 22, 61], [3, 1, 7, 17, 68], [2, 6, 12, 25, 54], [0, 2, 4, 13, 80], [4, 4, 4, 40, 46], [2, 0, 0, 13, 84], [2, 0, 8, 26, 62], [0, 0, 2, 9, 88], [0, 0, 11, 25, 62], [0, 2, 9, 25, 62], [2, 0, 7, 31, 58], [0, 0, 7, 17, 75]];
    var instrumentsMeta = [['Hosa Cable GTR210 Guitar Instrument Cable - 10 Foot', '7.98'], ['Dunlop Dual Design Straplok System, Silver', '10.22'], ['Nady MPF-6 6-Inch Clamp On Microphone Pop Filter', '15.97'], ['Tortex Guitar Pick, .50 Red, Pack of 12 (418P.50)', '3.25'], ['Ernie Ball 4036 White Polypro Strap', '5.57'], ['Planet Waves Pro Winder String Winder and Cutter', '6.92'], ['Jim Dunlop 83 Cg Dunlop Crvd Capo Gld', '9.92'], ['Fender 351 Premium Celluloid Guitar Picks, 12 Pack, Black Moto, Medium', '4.82'], ["D'Addario EXP15 Coated Phosphor Bronze Acoustic Guitar Strings, Extra Light, 10-47", '9.19'], ["D'Addario EXL110 Nickel Wound Electric Guitar Strings, Regular Light, 10-46", '1.96'], ['Dunlop Pickholder (picks not included)', '1.54'], ['Planet Waves Classic Series Instrument Cable, 15 feet', '8.55'], ["D'Addario EJ26 Phosphor Bronze Acoustic Guitar Strings, Custom Light, 11-52", '0.2'], ['Planet Waves Planet Lock Guitar Strap, Polypropylene, Black', '8.83'], ['Ernie Ball 2221 Regular Slinky Nickel Wound Set (10 - 46)', '3.15'], ['Dunlop 5005 Pick Holder, Each (5005)', '1.85'], ['On Stage XCG4 Tubular Guitar Stand with Velveteen Padding and Security Strap', '10.99'], ['On Stage Stands MS7701B Tripod Boom Microphone Stand', '24.95'], ['Protec Guitar Strap with Leather Ends and Pick Pocket, Black', '6.46'], ['Hosa 25FT Guitar Cable Rt Angl To Straight', '15.32'], ['Visual Sound One Spot Combo Pack', '28.35'], ["Musician's Gear Tubular Guitar Stand Black", '9.99'], ['Planet Waves Assorted Pearl Celluloid Guitar Picks, 10 pack, Light', '4.29'], ['Planet Waves Classic Series Instrument Cable with Right Angle Plug, 0.5 feet (3-pack)', '10.02'], ['Snark SN1 Guitar Tuner', '11.21'], ['Snark SN2 Guitar Tuner', '12.99'], ['Snark SN-5 Tuner for Guitar, Bass and Violin', '9.95'], ['Guitar Hanger Hook Holder Wall Mount Display - Fits all size Guitars, Bass, Mandolin, Banjo, etc.', '3.01'], ["D'Addario NS Micro Clip-On Tuner", '12.56'], ['Snark SN-8 Super Tight All Instrument Tuner', '11.64']];
    var instrumentsData = [instrumentsScores, instrumentsMeta];

    var cdScores = [[43, 11, 13, 14, 17], [3, 2, 3, 7, 83], [12, 6, 8, 18, 53], [18, 6, 8, 15, 51], [12, 5, 7, 12, 61], [3, 4, 4, 6, 80], [1, 2, 1, 11, 81], [3, 3, 7, 14, 71], [6, 4, 9, 16, 62], [4, 0, 2, 6, 85], [6, 7, 10, 18, 55], [1, 2, 6, 15, 73], [16, 5, 7, 18, 52], [5, 2, 7, 18, 66], [10, 3, 8, 18, 59], [4, 4, 7, 15, 68], [16, 11, 16, 20, 34], [3, 3, 7, 21, 63], [1, 0, 3, 5, 89], [12, 10, 12, 20, 43], [7, 5, 9, 20, 57], [1, 0, 3, 9, 84], [5, 2, 2, 10, 78], [17, 8, 9, 21, 42], [8, 5, 8, 19, 56], [9, 8, 10, 27, 44], [24, 5, 9, 22, 38], [47, 11, 11, 11, 18], [25, 8, 10, 20, 35], [5, 3, 8, 20, 62]];
    var cdMeta = [['Dark Side of the Moon', '9.99'], ['Nevermind', '3.99'], ['Wish You Were Here', '12.0'], ['Master of Puppets', '9.0'], ['Metallica', '18.47'], ['Led Zeppelin IV (aka ZOSO)', '9.0'], ['Revolver', '18.93'], ["Sgt. Pepper's Lonely Hearts Club Band", '12.99'], ['The Beatles (The White Album)', '19.88'], ['Abbey Road [Vinyl]', '29.99'], ['OK Computer', '6.99'], ['Kid A', '16.45'], ['Hybrid Theory', '6.6'], ['The Beatles 1', '14.99'], ['Lateralus', '11.99'], ['Come Away with Me', '9.95'], ['Stripped', '8.74'], ['Get Rich Or Die Tryin', '8.15'], ['Fallen', '8.62'], ['Meteora', '4.99'], ['St. Anger', '9.99'], ['In The Zone', '11.21'], ['American Idiot', '6.99'], ['How to Dismantle an Atomic Bomb', '28.33'], ['Breakaway', '6.95'], ['X&amp;Y', '6.96'], ['The Emancipation of Mimi', '19.05'], ['The Massacre', '10.98'], ['Confessions on a Dance Floor', '6.24'], ['No Title', '9.0']]
    var cdData = [cdScores, cdMeta];

    var top = document.getElementById("topSelect");
    var topChoice = top.options[top.selectedIndex].value;
    var topData = null;

    var bot = document.getElementById("botSelect");
    var botChoice = bot.options[bot.selectedIndex].value;
    var botData = null;

    switch(topChoice){
        case "instruments":
            topData = instrumentsData;
            break;
        case "cds":
            topData = cdData;
            break;
    }

    switch(botChoice){
        case "instruments":
            botData = instrumentsData;
            break;
        case "cds":
            botData = cdData;
            break;
    }

    makeBoxGrid("top", topData);
    makeBoxGrid("bot", botData);

}

function makeNumber(side, number, boxSize){
    
    var stars = "stars";
    if (number == 1){
        stars  = "star";
    }

    d3.select("#"+side+"BoxGrid").append("text")
    .attr("x", boxSize/4)
    .attr("y", (5.7-number) * boxSize)
    .text(number + " " + stars)

    d3.select("#"+side+"BoxGrid").append("line")
    .attr("x1", 0)
    .attr("y1", (number-1)*boxSize)
    .attr("x2", 2*boxSize)
    .attr("y2", (number-1)*boxSize)
    .attr("style", "stroke:rgb(0,0,0);stroke-width:0.2;")
}

function makeBoxGrid(side, myData){

    if(myData == null){
        alert("Something went wrong, null data!");
    }

    var myScores = myData[0];
    var myMeta = myData[1];

    var boxSize = 30;
    for (var i = 1; i <= 5; i++){
        makeNumber(side, i, boxSize);
    }

    // product #'s
    d3.select("#"+side+"BoxGrid").append("text")
    .attr("x", boxSize/4)
    .attr("y", 6 * boxSize)
    .text("Rank:");

    d3.select("#"+side+"BoxGrid").append("line")
    .attr("x1", 0)
    .attr("y1", 5*boxSize)
    .attr("x2", 2*boxSize)
    .attr("y2", 5*boxSize)
    .attr("style", "stroke:rgb(0,0,0);stroke-width:0.2;")

    // add the columns
    d3.select("#"+side+"BoxGrid").selectAll(".col")
        .data(myScores)
        .enter().append("g")
        .attr("class", "col")
        .attr("transform", function(d, i) {
            return "translate( " + boxSize*(i+2) + " 0)";
        })

    // add the boxes
    d3.select("#"+side+"BoxGrid").selectAll(".col").selectAll("rect")
      .data(function(d){return d;})
      .enter().append("rect")
      .attr("x", 0)
      .attr("y", function(d, i) {return boxSize*(4-i)})
      .attr("width", boxSize)
      .attr("height", boxSize)
      .attr("fill", function(d){
        
        // // low to high range of lightness is 100-->50
        // var p = 1 - (d/100);
        // value = 50 + p*p*50;
        
        // // return "hsl("+ 0 +", " + 100 + "%,"+ value + "%)";
        colors = ["#ffffff", 
        "#fcfbfd",
        "#efedf5",
        "#dadaeb",
        "#bcbddc",
        "#9e9ac8",
        "#807dba",
        "#6a51a3",
        "#54278f",
        "#3f007d"];

        if (d == 0){
            return colors[0];
        }
        if (d < 11){
            return colors[1];
        } 
        if (d < 22){
            return colors[2];
        }
        if (d < 33){
            return colors[3];
        }
        if (d < 44){
            return colors[4];
        }
        if (d < 55){
            return colors[5];
        }
        if (d < 66){
            return colors[6];
        }
        if (d < 77){
            return colors[7];
        }
        if (d < 88){
            return colors[8];
        }
        else {
            return colors[9];
        }
        })
      .attr("stroke", "black")
      .attr("stroke-width", 0.1)

    // make lines for the columns
    d3.select("#"+side+"BoxGrid").selectAll(".col")
    .append("line")
    .attr("x1",0)
    .attr("y1",0)
    .attr("x2",0)
    .attr("y2",5*boxSize)
    .attr("style", "stroke:rgb(0,0,0);stroke-width:0.5;")

    // make lines for the columns
    d3.select("#"+side+"BoxGrid").selectAll(".col")
    .append("line")
    .attr("x1",0)
    .attr("y1",5*boxSize)
    .attr("x2",0)
    .attr("y2",6.5*boxSize)
    .attr("style", "stroke:rgb(0,0,0);stroke-width:0.2;")

    // make the numbers for below the grid
    d3.select("#"+side+"BoxGrid").selectAll(".col").select(".text")
        .data(myScores)
        .enter().append("text")
        .attr("x",function(d,i){
            if (i < 9){
                return 2*boxSize + boxSize/3 + i*boxSize; 
            }
            return 2*boxSize + boxSize/4 + i*boxSize;
        })
        .attr("y",boxSize*6)
        .text(function(d,i){
            return i+1;
        })

    // hovering
    d3.select("#"+side+"BoxGrid").selectAll(".col")
      .data(myMeta)
      .attr("onmousemove", function(d,i){
          var name = d[0];
          var price = d[1];
          var data = myScores[i];

          var f = "popupInfo(" + "\"" + name + "\",\"" + price + "\",\"" + data + "\",\"" + side + "\")";
          return f;
        });

    d3.select("#"+side+"BoxGrid")
      .attr("onmouseleave", function(){
        return "mouseLeave(\"" + side + "\")";
      })
}

function mouseLeave(side){
    // remove old popout
    d3.select("#"+side+"BoxGrid")
    .selectAll(".popout")
    .remove();
  
}

function popupInfo(name, price, data, side){

    // numbers
    data = data.split(",");

    var svg = document.getElementById(side+"BoxGrid");

    var y = event.clientY - svg.getBoundingClientRect().top;
    var x = event.clientX - svg.getBoundingClientRect().left;

    // format price
    decimals = price.split(".");
    if (decimals[1].length < 2){
      price += "0";
    }
    price = "$" + price

    // remove old popout
    d3.select("#"+side+"BoxGrid")
      .selectAll(".popout")
      .remove();

    // add new popout
    d3.select("#"+side+"BoxGrid")
      .append("rect")
      .attr("width", function(){

          return Math.max(name.length,price.length) * 10 + 5;
      })
      .attr("height", 150)
      .attr("fill", "white")
      .attr("stroke", "black")
      .attr("fill-opacity", 1)
      .attr("x", x)
      .attr("y", y)
      .attr("class", "popout")
    
    // add text
    d3.select("#"+side+"BoxGrid")
      .append("text")
      .attr("x", x - 0 + 5)
      .attr("y", y - 0 + 20) // convert to int lol
      .attr("fill", "black")
      .attr("class", "popout")
      .attr("style", "font-family:\"Courier New\";")
      .text(name)

      d3.select("#"+side+"BoxGrid")
      .append("text")
      .attr("x", x - 0 + 5)
      .attr("y", y - 0 + 40) // convert to int lol
      .attr("fill", "black")
      .attr("class", "popout")
      .attr("style", "font-family:\"Courier New\";")
      .text(price)

      d3.select("#"+side+"BoxGrid")
      .append("text")
      .attr("x", x - 0 + 5)
      .attr("y", y - 0 + 60) // convert to int lol
      .attr("fill", "black")
      .attr("class", "popout")
      .attr("style", "font-family:\"Courier New\";")
      .text("5-star: " + data[4] + "%");

      d3.select("#"+side+"BoxGrid")
      .append("text")
      .attr("x", x - 0 + 5)
      .attr("y", y - 0 + 60) // convert to int lol
      .attr("fill", "black")
      .attr("class", "popout")
      .attr("style", "font-family:\"Courier New\";")
      .text("5-star: " + data[4] + "%");

      d3.select("#"+side+"BoxGrid")
      .append("text")
      .attr("x", x - 0 + 5)
      .attr("y", y - 0 + 80) // convert to int lol
      .attr("fill", "black")
      .attr("class", "popout")
      .attr("style", "font-family:\"Courier New\";")
      .text("4-star: " + data[3] + "%");

      d3.select("#"+side+"BoxGrid")
      .append("text")
      .attr("x", x - 0 + 5)
      .attr("y", y - 0 + 100) // convert to int lol
      .attr("fill", "black")
      .attr("class", "popout")
      .attr("style", "font-family:\"Courier New\";")
      .text("3-star: " + data[2] + "%");

      d3.select("#"+side+"BoxGrid")
      .append("text")
      .attr("x", x - 0 + 5)
      .attr("y", y - 0 + 120) // convert to int lol
      .attr("fill", "black")
      .attr("class", "popout")
      .attr("style", "font-family:\"Courier New\";")
      .text("2-star: " + data[1] + "%");

      d3.select("#"+side+"BoxGrid")
      .append("text")
      .attr("x", x - 0 + 5)
      .attr("y", y - 0 + 140) // convert to int lol
      .attr("fill", "black")
      .attr("class", "popout")
      .attr("style", "font-family:\"Courier New\";")
      .text("1-star: " + data[0] + "%");
}
