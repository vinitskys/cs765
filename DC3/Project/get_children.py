# Given a category ID, returns the top 30 children of the category
# sorted by decreasing total # of products

import csv
import sys

# convert all the datatypes appropriately
def convertCSVLineToDict(line):
    my_dict = {}
    my_dict["id"] = int(line[0])
    my_dict["name"] = line[1]
    my_dict["productCount"] = int(line[2])
    my_dict["subtreeProductCount"] = int(line[3])
    my_dict["parent"] = int(line[4])
    my_dict["numChildren"] = int(line[5])
    my_dict["pathName"] = eval(line[6])
    my_dict["children"] = eval(line[7])
    my_dict["alsoCount"] = int(line[8])
    my_dict["also"] = eval(line[9])
    my_dict["csv_line"] = line

    return my_dict

# get the root's dictionary from the csv file
def getRootDict(root_id, file_name):
    if root_id < 0:
        print("Error: Root ID must be at least 0")
        quit()

    csvFile = open(file_name)
    data = csv.reader(csvFile) # parse as CSV, so I don't have to deal with commas

    # remove header
    header = data.__next__() 

    for line in data:
        if int(line[0]) == root_id:
            return convertCSVLineToDict(line)
    
    print("Error: Root ID not found")
    return None



# get the top 30 children (as dicts)
def getTop30Children(root_id, file_name):
    
    root = getRootDict(root_id, file_name)
    if root == None:
        quit()

    csvFile = open(file_name)
    data = csv.reader(csvFile) # parse as CSV, so I don't have to deal with commas

    # remove header
    header = data.__next__() 

    children_ids = root["children"]

    # iterate through the data file
    children = {}
    for line in data:
        my_id = int(line[0])
        if my_id in children_ids:
            my_dict = convertCSVLineToDict(line)
            children[my_id] = my_dict

    # list of 30 0's
    top30 = []

    for i in range(30):
        best = None
        for child in children.values():
            if best == None:
                best = child
            else:
                if child["productCount"] + child["subtreeProductCount"] > best["productCount"] + best["subtreeProductCount"]:
                    best = child   
        
        if best != None:
            top30.append(best)
            children.pop(best["id"])
    
    return top30

def main():

    input_id = int(sys.argv[1])
    
    top30 = getTop30Children(input_id, "all-nodes.csv")
    
    for cat in top30:
        print(cat["csv_line"])

if __name__ == "__main__":
    main()